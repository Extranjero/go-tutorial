package handlers

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/Extranjero/go-tutorial/conf"
	"github.com/sirupsen/logrus"
	"github.com/jinzhu/gorm"
	"bitbucket.org/Extranjero/go-tutorial/utils"
	"bitbucket.org/Extranjero/go-tutorial/models"
)

var (
	cfg *conf.Config
	log *logrus.Logger
	db *gorm.DB
)

const (
	statusSuccess       = "success"
	statusError         = "error"
	statusFail          = "fail"
	errorIdValidation   = "data_validation"
	errorIdUnauthorized = "unauthorized"
)

type Resp struct {
	Status  string      `json:"status"`
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"data"`
}

func Get() *gin.Engine {
	cfg = conf.Get()
	log = utils.Get()
	db = models.Get()

	api := gin.Default()

	api_v1 := api.Group("/v1")
	api_v1.POST("/clients", ClientCreate)
	api_v1.PUT("/clients/:id", ClientUpdate)
	api_v1.POST("/drivers", DriverCreate)
	api_v1.PUT("/drivers/:id", DriverUpdate)
	api_v1.PUT("/orders", OrderCreate)

	return api
}