package handlers

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/Extranjero/go-tutorial/models"
	"net/http"
	"github.com/sirupsen/logrus"
	"database/sql"
)

type OrderJson struct {
	Id   int64 `json:"id"`
	ClientId int64 `json:"client_id"`
}

func OrderCreate(c *gin.Context) {
	req := OrderJson{}
	err := c.BindJSON(&req)
	if err != nil {
		log.WithFields(logrus.Fields{
			"error":    err,
			"method":   c.Request.Method,
			"path":     c.Request.RequestURI,
			"params":   c.Params,
		}).Error("bind json")

		c.JSON(http.StatusBadRequest, Resp{
			Status: statusFail,
			Message: "Invalid Json",
			Data: map[string]interface{}{
				"error_id": errorIdValidation,
			},
		})
		return
	}

	if req.ClientId == 0 {
		c.JSON(http.StatusBadRequest, Resp{
			Status: statusFail,
			Message: "Data validation",
			Data: map[string]interface{}{
				"error_id": errorIdValidation,
				"client_id": "This field is required",
			},
		})
		return
	}

	n_client_id := sql.NullInt64{
		Int64: req.ClientId,
		Valid: true,
	}

	order := models.Order{
		ClientID: n_client_id,
	}

	if err := db.Create(&order).Error; err != nil {
		log.WithFields(logrus.Fields{
			"error":    err,
			"method":   c.Request.Method,
			"path":     c.Request.RequestURI,
			"params":   c.Params,
		}).Error("create client")
		return
	}

	c.JSON(http.StatusOK, Resp{
		Status: statusSuccess,
		Data: order,
	})
}

