package models

import (
	"testing"
	"gopkg.in/stretchr/testify.v1/assert"
)

func TestOrder(t *testing.T) {
	db := Get()

	client := Client{
		Name: "Abdullo",
	}
	err := db.Create(&client).Error
	assert.NoError(t, err)

	driver := Driver{
		Name: "Abdullo",
	}
	err = db.Create(&driver).Error
	assert.NoError(t, err)

	order := Order{
		Client: client,
		Driver: driver,
		Status: 1,
	}
	err = db.Create(&order).Error
	assert.NoError(t, err)
}
