package handlers

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/Extranjero/go-tutorial/models"
	"net/http"
	"github.com/sirupsen/logrus"
	"strconv"
)

type DriverJson struct {
	Id   uint `json:"id"`
	Name string `json:"name"`
}

func DriverCreate(c *gin.Context) {
	req := DriverJson{}
	err := c.BindJSON(&req)
	if err != nil {
		log.WithFields(logrus.Fields{
			"error":    err,
			"method":   c.Request.Method,
			"path":     c.Request.RequestURI,
			"params":   c.Params,
		}).Error("bind json")

		c.JSON(http.StatusBadRequest, Resp{
			Status: statusFail,
			Message: "Invalid Json",
			Data: map[string]interface{}{
				"error_id": errorIdValidation,
			},
		})
		return
	}

	if req.Name == "" {
		c.JSON(http.StatusBadRequest, Resp{
			Status: statusFail,
			Message: "Data validation",
			Data: map[string]interface{}{
				"error_id": errorIdValidation,
				"name": "This field is required",
			},
		})
		return
	}

	client := models.Driver{
		Name: req.Name,
	}

	if err := db.Create(&client).Error; err != nil {
		log.WithFields(logrus.Fields{
			"error":    err,
			"method":   c.Request.Method,
			"path":     c.Request.RequestURI,
			"params":   c.Params,
		}).Error("create client")
		return
	}

	c.JSON(http.StatusOK, Resp{
		Status: statusSuccess,
		Data: client,
	})
}

func DriverUpdate(c *gin.Context) {
	req := DriverJson{}
	err := c.BindJSON(&req)
	if err != nil {
		log.WithFields(logrus.Fields{
			"error":    err,
			"method":   c.Request.Method,
			"path":     c.Request.RequestURI,
			"params":   c.Params,
		}).Error("bind json")

		c.JSON(http.StatusBadRequest, Resp{
			Status: statusFail,
			Message: "Invalid Json",
			Data: map[string]interface{}{
				"error_id": errorIdValidation,
			},
		})
		return
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, Resp{
			Status: statusFail,
			Message: "Invalid Id",
			Data: map[string]interface{}{
				"error_id": errorIdValidation,
			},
		})
		return
	}

	if req.Name == "" || id == 0 {
		c.JSON(http.StatusBadRequest, Resp{
			Status: statusFail,
			Message: "Data validation",
			Data: map[string]interface{}{
				"error_id": errorIdValidation,
				"name": "This field is required",
				"id": "This field is required",
			},
		})
		return
	}

	client := models.Driver{}
	client.ID = uint(id)

	if err := db.Model(&client).Update("Name", req.Name).Error; err != nil {
		log.WithFields(logrus.Fields{
			"error":    err,
			"method":   c.Request.Method,
			"path":     c.Request.RequestURI,
			"params":   c.Params,
		}).Error("update client")
		return
	}

	c.JSON(http.StatusOK, Resp{
		Status: statusSuccess,
		Data: client,
	})
}