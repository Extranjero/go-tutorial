package utils

import (
	"sync"

	"strings"

	"bitbucket.org/Extranjero/go-tutorial/conf"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
)

var (
	log  *logrus.Logger
	once sync.Once
)

func Get() *logrus.Logger {
	once.Do(func() {
		cfg := conf.Get()
		log = logrus.New()

		log.Level = setLevel(cfg.Log.Level)

		if !cfg.Log.ToFile {
			return
		}

		log.Formatter = &logrus.TextFormatter{
			DisableColors: true,
		}

		log.Hooks.Add(lfshook.NewHook(lfshook.PathMap{
			logrus.DebugLevel: cfg.Log.Path + "debug.log",

			logrus.InfoLevel: cfg.Log.Path + "info.log",
			logrus.WarnLevel: cfg.Log.Path + "info.log",

			logrus.ErrorLevel: cfg.Log.Path + "error.log",
			logrus.FatalLevel: cfg.Log.Path + "error.log",
			logrus.PanicLevel: cfg.Log.Path + "error.log",
		}))
	})

	return log
}

func setLevel(level string) logrus.Level {
	switch strings.ToLower(level) {
	case "debug":
		return logrus.DebugLevel
	case "info":
		return logrus.InfoLevel
	case "error":
		return logrus.ErrorLevel
	case "warn":
		return logrus.WarnLevel
	case "fatal":
		return logrus.FatalLevel
	case "panic":
		return logrus.PanicLevel
	default:
		return logrus.InfoLevel
	}
}