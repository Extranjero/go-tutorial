package conf

import (
	"io/ioutil"
	"os"
	"sync"

	"gopkg.in/yaml.v2"
)

var (
	confFileName = "conf.yaml"
	confPath = "conf" + string(os.PathSeparator)
	cfg  *Config
	once sync.Once
)

type Database struct {
	Dialect  string `yaml:"dialect"`
	Host     string `yaml:"host"`
	DbName   string `yaml:"dbname"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Port     string `yaml:"port"`
	SSL      string `yaml:"ssl"`
	SqlDir   string `yaml:"sql_dir"`
	Location string `yaml:"location"`
}

type Config struct {
	Log      struct {
			 Path   string `yaml:"path"`
			 Level  string `yaml:"level"`
			 ToFile bool   `yaml:"to_file"`
		 } `yaml:"log"`

	Database Database `yaml:"database"`

	Address  string `yaml:"address"`
}

func (c *Config) SetDefaults() {
	c.Log.Path = "/var/log/golang/"
	c.Log.Level = "info"

	c.Database.Dialect = "mysql"
	c.Database.DbName = "golang"
	c.Database.Host = "localhost"
	c.Database.Port = "3306"
	c.Database.User = "root"
	c.Database.Password = "root"
	c.Database.SSL = "disable"
	c.Database.SqlDir = "/home/go/src/bitbucket.org/mytaxi/notification-service/sql"
	c.Database.Location = "Asia/Tashkent"

	c.Address = ":9000"
}

func Get() *Config {
	once.Do(func() {
		var err error
		filePath := confPath + confFileName
		cfg, err = NewConfig(filePath)
		if err != nil {
			panic(err)
		}
	})

	return cfg
}

func NewConfig(filePath string) (cf *Config, err error) {
	cf = new(Config)
	cf.SetDefaults()

	var fileBytes []byte
	fileBytes, err = ioutil.ReadFile(filePath)
	if err != nil {
		return
	}

	err = yaml.Unmarshal(fileBytes, cf)
	if err != nil {
		return
	}

	return
}

