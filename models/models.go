package models

import (
	"github.com/jinzhu/gorm"
	"database/sql"
)

type Client struct {
	gorm.Model

	Name string
}

type Driver struct {
	gorm.Model

	Name string
}

type Order struct {
	gorm.Model

	Client Client
	ClientID sql.NullInt64

	Driver Driver
	DriverID sql.NullInt64

	Status uint
}
