package main

import (
	"bitbucket.org/Extranjero/go-tutorial/conf"
	"bitbucket.org/Extranjero/go-tutorial/handlers"
)

func main() {
	cfg := conf.Get()

	api := handlers.Get()
	api.Run(cfg.Address)
}
