package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"sync"
	"bitbucket.org/Extranjero/go-tutorial/conf"
	"fmt"
)

var once sync.Once
var db *gorm.DB

func Get() *gorm.DB {
	once.Do(func() {
		cf := conf.Get()
		var err error

		dsn := fmt.Sprintf("%s:%s@tcp(%s:%v)/%s",
			cf.Database.User,
			cf.Database.Password,
			cf.Database.Host,
			cf.Database.Port,
			cf.Database.DbName)
		db, err = gorm.Open(cf.Database.Dialect, dsn)
		if err != nil {
			panic(fmt.Sprintf("Failed connect to database dialect=%s dsn=%s err=%s",
				cf.Database.Dialect,
				dsn,
				err,
			))
		}

		autoMigrate()
	})
	return db
}

var err error

func autoMigrate(){
	var err error
	migrate(&Client{}, err)
	migrate(&Driver{}, err)
	migrate(&Order{}, err)

	if err != nil {
		panic(err)
	}
}

func migrate(model interface{}, err error) {
	if err != nil {
		return
	}

	err = db.AutoMigrate(model).Error
}
